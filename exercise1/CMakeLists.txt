cmake_minimum_required(VERSION 3.17)
project(exercise1 C)

set(CMAKE_C_STANDARD 99)

add_executable(exercise1 main.c)