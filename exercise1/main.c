/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/21/2020
 */

#include <stdio.h>

#define MAX_MATRIX_ROWS 50
#define MAX_MATRIX_COLUMNS 50
#define WHITE 0
#define BLACK 1

// Struttura rappresentante un'entrata della matrice.
typedef struct {
    int x;
    int y;
    int width;
    int height;
    int maxValue;
} cell;

void printCell(cell value, char type[]);
int leggiMatrice(int M[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int *nr, int *nc);
int riconosciRegione(int M[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int nr, int nc, int r, int c, int *b, int *h);

int main() {
    int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS];
    int rows, columns;
    cell maxWidth = {-1, -1, 0, 0, 0};
    cell maxHeight = {-1, -1, 0, 0, 0};
    cell maxSquare = {-1, -1, 0, 0, 0};
    cell tmpCell;

    // Se la funzione ritorna un valore diverso da 0, significa che c'è un errore con il file da leggere.
    if (leggiMatrice(matrix, &rows, &columns) != 1) {
        printf("Invalid file.");
        return -1;
    }

    // Per ogni entrata della matrice, richiamo la funzione riconosciRegione
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            // Imposto gli indici dell'entrata corrente nella struct.
            tmpCell.x = i;
            tmpCell.y = j;
            // Se l'entrata corrente è l'inizio di una sequenza, controllo se essa è massima in larghezza/altezza/area
            // e in tal caso la salvo.
            if (riconosciRegione(matrix, rows, columns, i, j, &tmpCell.width, &tmpCell.height)) {
                if (tmpCell.width > maxWidth.maxValue) {
                    tmpCell.maxValue = tmpCell.width;
                    maxWidth = tmpCell;
                }

                if (tmpCell.height > maxHeight.maxValue) {
                    tmpCell.maxValue = tmpCell.height;
                    maxHeight = tmpCell;
                }

                if ((tmpCell.width * tmpCell.height) > maxSquare.maxValue) {
                    tmpCell.maxValue = tmpCell.width * tmpCell.height;
                    maxSquare = tmpCell;
                }
            }
        }
    }

    // Stampo la cella massima per ogni categoria.
    printCell(maxWidth, "Base");
    printCell(maxHeight, "Altezza");
    printCell(maxSquare, "Area");

    return 0;
}

// Legge una matrice da file, ritornando per mezzo di puntatori a interi il numero effettivo di righe e colonne.
int leggiMatrice(int M[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int *nr, int *nc)
{
    char filename[31];
    FILE *fi;

    printf("Insert the input file name: ");
    scanf("%s", filename);

    fi = fopen(filename, "r");

    // Se c'è un problema con il file ritorno 0 (false) ed esco; ci penserà il main a gestire l'errore.
    if (fi == NULL){
        return 0;
    }

    // Leggo i valori del numero di righe e colonne e li salvo nei valori puntati dai due puntatori passati come parametri.
    fscanf(fi, "%d%d", nr, nc);

    // Leggo la matrice da file.
    for (int i = 0; i < *nr; i++) {
        for (int j = 0; j < *nc; j++) {
            fscanf(fi, "%d", &M[i][j]);
        }
    }

    // Ritorno 1: il processo è stato completato senza alcun errore.
    return 1;
}

// Riconosce se la cella individuata dalle coordinate passate come parametro è l'inizio di una regione (ritornerà 1)
// o meno (ritornerà 0).
int riconosciRegione(int M[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int nr, int nc, int r, int c, int *b, int *h)
{
    *b = 0;
    *h = 0;
    int white = 0;

    // Se la cella non è nera, sicuramente non apparterrà ad una regione.
    if (M[r][c] != BLACK) {
        return 0;
    }

    // Una cella può essere angolo sinistro in alto di una regione solo se:
    // 1) è in posizione (0,0)
    // 2) è in posizione (x, y) e (x-1, y) è bianca e (x, y-1) è bianca.
    // Ricordiamo che arrivati a questo punto la cella è per forza nera poichè in caso contrario la funzione avrebbe
    // ritornato il valore 0 e non sarebbe arrivata a questa selezione.
    if ((r > 0 && M[r - 1][c] != WHITE) || (c > 0 && M[r][c - 1] != WHITE)) {
        return 0;
    }

    // Scorro la riga finchè non trovo una cella bianca: trovo l'altezza.
    for (int k = r; (k < nr && !white); k++) {
        if (M[k][c] == WHITE || k == nr - 1) {
            *h = k - r + ( (k == nr - 1) ? 1 : 0);

            white = 1;
        }
    }

    white = 0;

    // Scorro la colonna finchè non trovo una cella bianca: trovo la base
    for (int l = c; (l < nc && !white); l++) {
        if (M[r][l] == WHITE || l == nc - 1) {
            *b = l - c + ( (l == nc - 1) ? 1 : 0);

            white = 1;
        }
    }

    // Ritorno 1: tutto è andato per il meglio e la cella rappresenta l'inizio di una regione nera.
    return 1;
}

// Metodo per stampare le informazioni riguardanti una cella in maniera più agevole: è superfluo ma lo trovo molto
// più elegante: il codice rimane diviso e pulito.
void printCell(cell value, char type[])
{
    printf("Max %s: estr. sup. SX=<%d, %d>, b=%d, h=%d, Area=%d\n", type, value.x, value.y, value.width, value.height, value.width * value.height);
}
