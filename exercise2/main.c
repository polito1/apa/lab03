/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/20/2020
 */

#include <stdio.h>

// long double = 16 bytes * 8 bits = 128 bits
#define MAX_BITS_LENGTH 128
// In ogni byte ci sono 8 bits
#define BITS_IN_BYTE 8

void stampaCodifica(void *p, int size, int bigEndian);
int getPadding(int size);

// Il formato è standard IEEE 754, quindi possiamo definire delle costanti che ne salvano i formati per i vari tipi di
// variabile.
typedef struct {
    int sign;
    int exp;
    int mantissa;
    int signBits;
    int expBits;
    int mantissaBits;
} configuration;

const configuration floatConfig = {
        0,
        1,
        9,
        1,
        8,
        23
};

const configuration doubleConfig = {
        0,
        1,
        12,
        1,
        11,
        52
};

const configuration longDoubleConfig = {
        0,
        1,
        16,
        1,
        15,
        112
};

int main() {
    float af;
    double ad;
    long double ald;
    int bigEndian;
    unsigned int checkBigEndian = 1;
    char *firstChar = (char *) &checkBigEndian;

    // Se *firstChar == 1, si tratta di una codifica bigEndian, altrimenti littleEndian.
    bigEndian = ((int) *firstChar);

    // Prendo il numero in input.
    printf("Inserire il numero da codificare: ");
    scanf("%f", &af);
    // Assegno il numero alle altre due variabili (utilizzando il casting).
    ad = (double) af;
    ald = (long double) af;

    // Richiamo la funzione.
    stampaCodifica((void *) &af, sizeof(af), bigEndian);
    stampaCodifica((void *) &ad, sizeof(ad), bigEndian);
    stampaCodifica((void *) &ald, sizeof(ald), bigEndian);

    return 0;
}

// Stampa la codifica del numero inserito.
void stampaCodifica(void *p, int size, int bigEndian)
{
    unsigned int binary[MAX_BITS_LENGTH] = {0}, currentBit;
    unsigned char *pointer;
    // Casting del puntatore.
    pointer = (unsigned char*) p;
    configuration config;
    int outputCounter = 0;
    // Calcolo il padding
    int padding = getPadding(size), increment = 1;

    // Se c'è stato un errore durante la lettura del padding, esco dalla funzione.
    if (padding == -1) {
        return;
    }

    // Stampo le informazioni e assegno la struct configurazione.
    switch (size) {
        case 4:
            config = floatConfig;
            printf("Variabile float su %d bytes. Padding: %d bits\n", size, padding);
            printf("Segno: %d bits, esponente: %d bits, mantissa: %d bits.\n", config.signBits, config.expBits, config.mantissaBits);
            break;
        case 8:
            config = doubleConfig;
            printf("Variabile double su %d bytes. Padding: %d bits\n", size, padding);
            printf("Segno: %d bits, esponente: %d bits, mantissa: %d bits.\n", config.signBits, config.expBits, config.mantissaBits);
            break;
        case 16:
            config = longDoubleConfig;
            printf("Variabile long double su %d bytes. Padding: %d bits\n", size, padding);
            printf("Segno: %d bits, esponente: %d bits, mantissa: %d bits.\n", config.signBits, config.expBits, config.mantissaBits);
            break;
        default:
            // Se la size non è 4, 8 o 16, c'è un problema, esco dalla funzione.
            return;
    }

    // Se la codifica è littleEndian, dovrò leggere i bit dal fondo.
    if (!bigEndian) {
        pointer += size;

        increment = -1;
    }

    // Per ogni byte,
    for (int byte = 0; byte < size; byte++) {
        // per ogni bit,
        for (int bit = 0; bit < BITS_IN_BYTE; bit++) {
            // Converto il byte in bits tramite il metodo dei resti.
            currentBit = *pointer % 2;
            *pointer = *pointer / 2;

            // Salvo in un array l'output.
            binary[byte * BITS_IN_BYTE + bit] = currentBit;
        }

        // Incremento il puntatore.
        pointer += increment;
    }

    // Stampo la formattazione.
    printf("s\texp %*c\tmantissa\n", config.expBits - 4, ' ');

    // Per ogni bit nell'array
    for (int i = size*8 -1; i >= 0; i--) {
        // Se non è un bit di padding
        if (outputCounter >= padding) {
            // Dividi in segno, esponente e mantissa l'output.
            if (
                    outputCounter == config.exp + padding
                    || outputCounter == config.mantissa + padding
                    ) {
                printf("\t");
            }

            // Stampo il bit
            printf("%d", binary[i]);
        }
        // Incremento il contatore.
        outputCounter++;
    }

    printf("\n\n");
}

// Funzione per ottenere i bit di padding.
int getPadding(int size)
{
    int count = 0;
    unsigned char *pointer, *pointer2;

    // Per ogni size dovrò creare due variabili del tipo corretto con valore discorde, in modo da avere il primo bit
    // (quello di segno) differente, e poter calcolare i bits di padding.
    if (size == 4) {
        float a = 1;
        float b = -1;
        pointer = (unsigned char*) &a;
        pointer2 = (unsigned char*) &b;
    } else if (size == 8) {
        double a = 1;
        double b = -1;
        pointer = (unsigned char*) &a;
        pointer2 = (unsigned char*) &b;
    } else if (size == 16) {
        long double a = 1;
        long double b = -1;
        pointer = (unsigned char*) &a;
        pointer2 = (unsigned char*) &b;
    } else {
        // Se la size non è 4, 8 o 16, c'è un problema, ritorno -1, un valore impossibile per una lunghezza.
        return -1;
    }

    // Finchè i puntatori puntano allo stesso valore si tratta di padding
    while(*pointer == *pointer2){
        pointer++;
        pointer2++;
        count++;
    }

    // Ritorno i bits di padding.
    return (size - count - 1) * BITS_IN_BYTE;
}
